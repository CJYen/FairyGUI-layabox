import Main from "./MainMenu/Main";
import MainMenuBinder from "./MainMenu/MainMenuBinder";
import SkeletonHandler from "../common/SkeletonHandler";

export default class MainMenu {
    private _view: fgui.GComponent;
    private skeletonHandler: SkeletonHandler;

    constructor() {
        fgui.UIPackage.loadPackage("res/UI/MainMenu", Laya.Handler.create(this, this.onUILoaded));
    }

    onUILoaded() {
        this._view = fgui.UIPackage.createObject("MainMenu", "Main").asCom;
        this._view.makeFullScreen();
        fgui.GRoot.inst.addChild(this._view);

       
    }

    startDemo(demoClass: any): void {
        this._view.dispose();
        let demo: any = new demoClass();
        Laya.stage.event("start_demo", demo);
    }

    // /**
    //  * @api {function} initSkeletonHandler
    //  * @apiName initSkeletonHandler
    //  * @apiGroup SelectRoomHandler
    //  * @apiDescription initSkeletonHandler
    //  */
    // private async initSkeletonHandler(): Promise<void> {
    //     let { imgBgGirl, dragonBone } = this._view.comBgGirl;
    //     if (imgBgGirl.visible) return Promise.resolve();
    //     let { x, y, pivotX, pivotY, scaleX, scaleY } = dragonBone;
    //     let { niuniuGirl } = skeletonData;
    //     let config = niuniuGirl.lobby;
    //     config.pos = { x: x, y: y };
    //     config.pivot = { x: pivotX, y: pivotY };
    //     config.scale = { x: scaleX, y: scaleY };
    //     this.skeletonHandler = new SkeletonHandler(this._view.comBgGirl);
    //     await this.skeletonHandler.loadWithSkeleton(config);
    // }    

    destroy() {
        this._view.dispose();
    }
}