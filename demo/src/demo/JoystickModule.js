export default class JoystickModule extends Laya.EventDispatcher {
    constructor(mainView) {
        super();
        this._button = mainView.getChild("joystick").asButton;
        this._button.changeStateOnClick = false;
        this._thumb = this._button.getChild("thumb");
        this._touchArea = mainView.getChild("joystick_touch");
        this._center = mainView.getChild("joystick_center");
        this._InitX = this._center.x + this._center.width / 2;
        this._InitY = this._center.y + this._center.height / 2;
        this.touchId = -1;
        this.radius = 150;
        this._curPos = new Laya.Point();
        this._touchArea.on(Laya.Event.MOUSE_DOWN, this, this.onTouchDown);
    }
    Trigger(evt) {
        this.onTouchDown(evt);
    }
    onTouchDown(evt) {
        if (this.touchId == -1) { //First touch
            this.touchId = evt.touchId;
            if (this._tweener != null) {
                this._tweener.kill();
                this._tweener = null;
            }
            fgui.GRoot.inst.globalToLocal(Laya.stage.mouseX, Laya.stage.mouseY, this._curPos);
            var bx = this._curPos.x;
            var by = this._curPos.y;
            this._button.selected = true;
            if (bx < 0)
                bx = 0;
            else if (bx > this._touchArea.width)
                bx = this._touchArea.width;
            if (by > fgui.GRoot.inst.height)
                by = fgui.GRoot.inst.height;
            else if (by < this._touchArea.y)
                by = this._touchArea.y;
            this._lastStageX = bx;
            this._lastStageY = by;
            this._startStageX = bx;
            this._startStageY = by;
            this._center.visible = true;
            this._center.x = bx - this._center.width / 2;
            this._center.y = by - this._center.height / 2;
            this._button.x = bx - this._button.width / 2;
            this._button.y = by - this._button.height / 2;
            var deltaX = bx - this._InitX;
            var deltaY = by - this._InitY;
            var degrees = Math.atan2(deltaY, deltaX) * 180 / Math.PI;
            this._thumb.rotation = degrees + 90;
            Laya.stage.on(Laya.Event.MOUSE_MOVE, this, this.OnTouchMove);
            Laya.stage.on(Laya.Event.MOUSE_UP, this, this.OnTouchUp);
        }
    }
    OnTouchUp(evt) {
        if (this.touchId != -1 && evt.touchId == this.touchId) {
            this.touchId = -1;
            this._thumb.rotation = this._thumb.rotation + 180;
            this._center.visible = false;
            this._tweener = fgui.GTween.to2(this._button.x, this._button.y, this._InitX - this._button.width / 2, this._InitY - this._button.height / 2, 0.3)
                .setTarget(this._button, this._button.setXY)
                .setEase(fgui.EaseType.CircOut)
                .onComplete(this.onTweenComplete, this);
            Laya.stage.off(Laya.Event.MOUSE_MOVE, this, this.OnTouchMove);
            Laya.stage.off(Laya.Event.MOUSE_UP, this, this.OnTouchUp);
            this.event(JoystickModule.JoystickUp);
        }
    }
    onTweenComplete() {
        this._tweener = null;
        this._button.selected = false;
        this._thumb.rotation = 0;
        this._center.visible = true;
        this._center.x = this._InitX - this._center.width / 2;
        this._center.y = this._InitY - this._center.height / 2;
    }
    OnTouchMove(evt) {
        if (this.touchId != -1 && evt.touchId == this.touchId) {
            var bx = Laya.stage.mouseX;
            var by = Laya.stage.mouseY;
            var moveX = bx - this._lastStageX;
            var moveY = by - this._lastStageY;
            this._lastStageX = bx;
            this._lastStageY = by;
            var buttonX = this._button.x + moveX;
            var buttonY = this._button.y + moveY;
            var offsetX = buttonX + this._button.width / 2 - this._startStageX;
            var offsetY = buttonY + this._button.height / 2 - this._startStageY;
            var rad = Math.atan2(offsetY, offsetX);
            var degree = rad * 180 / Math.PI;
            this._thumb.rotation = degree + 90;
            var maxX = this.radius * Math.cos(rad);
            var maxY = this.radius * Math.sin(rad);
            if (Math.abs(offsetX) > Math.abs(maxX))
                offsetX = maxX;
            if (Math.abs(offsetY) > Math.abs(maxY))
                offsetY = maxY;
            buttonX = this._startStageX + offsetX;
            buttonY = this._startStageY + offsetY;
            if (buttonX < 0)
                buttonX = 0;
            if (buttonY > fgui.GRoot.inst.height)
                buttonY = fgui.GRoot.inst.height;
            this._button.x = buttonX - this._button.width / 2;
            this._button.y = buttonY - this._button.height / 2;
            this.event(JoystickModule.JoystickMoving, degree);
        }
    }
}
JoystickModule.JoystickMoving = "JoystickMoving";
JoystickModule.JoystickUp = "JoystickUp";
