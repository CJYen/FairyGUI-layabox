/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

import comBgGirl from "./comBgGirl";
import Button1 from "./Button1";

export default class Main extends fgui.GComponent {

	public imgBg:fgui.GImage;
	public imgRoomTop:fgui.GImage;
	public imgRefGirl:fgui.GImage;
	public comBgGirl:comBgGirl;
	public b0:Button1;

	public static URL:string = "ui://58oxr1hsrftu0";

	public static createInstance():Main {
		return <Main><any>(fgui.UIPackage.createObject("MainMenu","Main"));
	}

	public constructor() {
		super();
	}

	protected onConstruct(): void {
		this.imgBg = <fgui.GImage><any>(this.getChild("imgBg"));
		this.imgRoomTop = <fgui.GImage><any>(this.getChild("imgRoomTop"));
		this.imgRefGirl = <fgui.GImage><any>(this.getChild("imgRefGirl"));
		this.comBgGirl = <comBgGirl><any>(this.getChild("comBgGirl"));
		this.b0 = <Button1><any>(this.getChild("b0"));
	}
}