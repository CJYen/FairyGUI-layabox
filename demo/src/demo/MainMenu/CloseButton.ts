/** This is an automatically generated class by FairyGUI. Please do not modify it. **/



export default class CloseButton extends fgui.GButton {

	public button:fgui.Controller;
	public n1:fgui.GImage;
	public n2:fgui.GImage;

	public static URL:string = "ui://58oxr1hsrftu3";

	public static createInstance():CloseButton {
		return <CloseButton><any>(fgui.UIPackage.createObject("MainMenu","CloseButton"));
	}

	public constructor() {
		super();
	}

	protected onConstruct(): void {
		this.button = this.getController("button");
		this.n1 = <fgui.GImage><any>(this.getChild("n1"));
		this.n2 = <fgui.GImage><any>(this.getChild("n2"));
	}
}