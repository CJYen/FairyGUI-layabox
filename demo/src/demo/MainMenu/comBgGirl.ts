/** This is an automatically generated class by FairyGUI. Please do not modify it. **/



export default class comBgGirl extends fgui.GComponent {

	public imgBgGirl:fgui.GImage;
	public dragonBone:fgui.GGraph;

	public static URL:string = "ui://58oxr1hsft0fd";

	public static createInstance():comBgGirl {
		return <comBgGirl><any>(fgui.UIPackage.createObject("MainMenu","comBgGirl"));
	}

	public constructor() {
		super();
	}

	protected onConstruct(): void {
		this.imgBgGirl = <fgui.GImage><any>(this.getChild("imgBgGirl"));
		this.dragonBone = <fgui.GGraph><any>(this.getChild("dragonBone"));
	}
}