/** This is an automatically generated class by FairyGUI. Please do not modify it. **/



export default class Button1 extends fgui.GButton {

	public n7:fgui.GGraph;
	public title:fgui.GTextField;

	public static URL:string = "ui://58oxr1hsf5o9e";

	public static createInstance():Button1 {
		return <Button1><any>(fgui.UIPackage.createObject("MainMenu","Button1"));
	}

	public constructor() {
		super();
	}

	protected onConstruct(): void {
		this.n7 = <fgui.GGraph><any>(this.getChild("n7"));
		this.title = <fgui.GTextField><any>(this.getChild("title"));
	}
}