/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

import Button1 from "./Button1";
import comBgGirl from "./comBgGirl";
import Main from "./Main";
import CloseButton from "./CloseButton";

export default class MainMenuBinder{
	public static bindAll():void {
		fgui.UIObjectFactory.setExtension(Button1.URL, Button1);
		fgui.UIObjectFactory.setExtension(comBgGirl.URL, comBgGirl);
		fgui.UIObjectFactory.setExtension(Main.URL, Main);
		fgui.UIObjectFactory.setExtension(CloseButton.URL, CloseButton);
	}
}