export default class BagDemo {
    constructor() {
        fgui.UIPackage.loadPackage("res/UI/Bag", Laya.Handler.create(this, this.onUILoaded));
    }
    onUILoaded() {
        this._view = fgui.UIPackage.createObject("Bag", "Main").asCom;
        this._view.makeFullScreen();
        fgui.GRoot.inst.addChild(this._view);
        this._bagWindow = new BagWindow();
        this._view.getChild("bagBtn").onClick(this, () => { this._bagWindow.show(); });
    }
    destroy() {
        fgui.UIPackage.removePackage("Bag");
    }
}
class BagWindow extends fgui.Window {
    constructor() {
        super();
    }
    onInit() {
        this.contentPane = fgui.UIPackage.createObject("Bag", "BagWin").asCom;
        this.center();
    }
    onShown() {
        var list = this.contentPane.getChild("list").asList;
        list.on(fgui.Events.CLICK_ITEM, this, this.onClickItem);
        list.itemRenderer = Laya.Handler.create(this, this.renderListItem, null, false);
        list.setVirtual();
        list.numItems = 45;
    }
    renderListItem(index, obj) {
        obj.icon = "res/icons/i" + Math.floor(Math.random() * 10) + ".png";
        obj.text = "" + Math.floor(Math.random() * 100);
    }
    onClickItem(item) {
        this.contentPane.getChild("n11").asLoader.url = item.icon;
        this.contentPane.getChild("n13").text = item.icon;
    }
}
