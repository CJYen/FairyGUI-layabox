import Templet = Laya.Templet;
import Skeleton = Laya.Skeleton;
import Handler = Laya.Handler;
import Event = Laya.Event;

interface ISkeletonData {
    url: string,
    aniName: string|number,
    pos: { x: number, y: number },    
    pivot: { x: number, y: number },
    scale: { x: number, y: number },
    loop: boolean, 
    autoPlay: boolean
}

/**
 * @api {class} SkeletonHandler
 * @apiName SkeletonHandler
 * @apiGroup SkeletonHandler
 * @apiDescription Skeleton Handler
 */
export default class SkeletonHandler {
    private mRoot: fgui.GObject;
    private mFactory: Templet;
    private mArmature: Skeleton;
    private mAniMode: number;
    // private mAniPath: string;
    // private mAniName: string;

    constructor(root: fgui.GObject, aniMode?: number) {
        this.mRoot = root;
        this.mAniMode = aniMode || 0;
    }

    /**
     * @api {function} loadWithTemplet
     * @apiName loadWithTemplet
     * @apiGroup SkeletonHandler
     * @apiDescription load skeleton with Laya.Templet
     */
    public async loadWithTemplet(skeletonData: ISkeletonData): Promise<void> {
        let promise = new Promise<void>(resolve => {
            let args = this.combinArgs(skeletonData)
            this.mFactory = new Templet();
            this.mFactory.on(Event.COMPLETE, this, () => resolve(this.parseTempletComplete(args[0])), args);
            this.mFactory.loadAni(skeletonData.url);
        });
        return promise;
    }

    /**
     * @api {function} loadWithSkeleton
     * @apiName loadWithSkeleton
     * @apiGroup SkeletonHandler
     * @apiDescription load skeleton with Laya.Skeleton
     */
    public async loadWithSkeleton(skeletonData: ISkeletonData): Promise<void> {
        let promise = new Promise<void>(resolve => {
            let args = this.combinArgs(skeletonData)
            this.mArmature = new Skeleton();
            this.mArmature.load(
                skeletonData.url, Handler.create(this, () => resolve(this.parseSkeletonComplete(args[0])), args), this.mAniMode);
        });
        return promise;
    }

    /**
     * @api {function} dispose
     * @apiName dispose
     * @apiGroup SkeletonHandler
     * @apiDescription dispose SkeletonHandler
     */
    public dispose(): void {
        if (this.mFactory) this.mFactory.destroy();
        if (this.mArmature) this.mArmature.destroy(false);
        this.mRoot = null;
        this.mFactory = null;
        this.mArmature = null;
    }

    /**
     * @api {function} parseTempletComplete
     * @apiName parseTempletComplete
     * @apiGroup SkeletonHandler
     * @apiDescription on load skeleton with Laya.Templet complete
     */
    private parseTempletComplete(evt: ISkeletonData): void {
        let { pos, pivot, scale, aniName, loop, autoPlay } = evt;
        if (!this.mRoot) return;
        this.mArmature = this.mFactory.buildArmature(this.mAniMode);
        this.mRoot.displayObject.addChild(this.mArmature);
        this.mArmature.pivot(pivot.x, pivot.y).scale(scale.x, scale.y).pos(pos.x , pos.y);
        if (autoPlay) {
            this.showDragonBoneAni(aniName, loop);
        }
        else {
            this.mArmature.stop();
        }
    }

    /**
     * @api {function} parseSkeletonComplete
     * @apiName parseSkeletonComplete
     * @apiGroup SkeletonHandler
     * @apiDescription on load skeleton with Laya.Skeleton complete
     */
    private parseSkeletonComplete(evt: ISkeletonData): void {
        let { pivot, scale, aniName, loop, autoPlay } = evt;
        if (!this.mRoot) return;
        this.mRoot.displayObject.addChild(this.mArmature);
        this.mArmature.pivot(pivot.x, pivot.y).scale(scale.x, scale.y).pos(this.mRoot.width / 2 , this.mRoot.height / 2);
        if (autoPlay) {
            this.showDragonBoneAni(aniName, loop);
        }
        else {
            this.mArmature.stop();
        }
    }

    /**
     * @api {function} showDragonBoneAni
     * @apiName showDragonBoneAni
     * @apiGroup SkeletonHandler
     * @apiDescription show dragonbone animation winth name or index
     */
    private showDragonBoneAni(aniName: string | number, loop: boolean): void {
        if(this.mArmature) {
            this.mArmature.play(aniName, loop);
        }
    }

    /**
     * @api {function} combinArgs
     * @apiName combinArgs
     * @apiGroup SkeletonHandler
     * @apiDescription combin loader args
     */
    private combinArgs(skeletonData: ISkeletonData): Array<any> {
        let args = {
            pos: skeletonData.pos || { x:1, y:1},
            pivot: skeletonData.pivot || { x: 1, y: 1} ,
            scale: skeletonData.scale || { x: 1, y: 1} ,
            aniName: skeletonData.aniName || 0,
            loop: skeletonData.loop || false,
            autoPlay: skeletonData.autoPlay || false
        }
        return [args];
    }

}